import Vue from 'vue'
import Router from 'vue-router'

// Components
import Home from '../components/pages/Home'
import Grid from '../components/pages/Grid'
import PostList from '../components/pages/PostList'

Vue.use(Router)

const router = new Router({
    routes: [{
            path: '/',
            name: 'Home',
            component: Home,
        },
        {
            path: '/grid',
            name: 'Grid',
            component: Grid,
        },
        {
            path: '/postlist',
            name: 'postlist',
            component: PostList,
        }
    ],
    mode: 'history',
    base: 'corpblog',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    }
})

export default router