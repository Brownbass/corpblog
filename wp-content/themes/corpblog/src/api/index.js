let base_path = "http://localhost/corpblog/wp-json/wp/v2/";


export default {
    getCategories(cb) {
        axios.get(base_path + 'categories?sort=name&hide_empty=true&per_page=50')
            .then(response => {
                cb(response.data.filter(c => c.name !== "Uncategorized"))
            })
            .catch(e => {
                cb(e)
            })
    },

    getPages(cb) {
        axios.get(base_path + 'pages?per_page=10')
            .then(response => {
                cb(response.data)
            })
            .catch(e => {
                cb(e)
            })
    },

    getPage(id, cb) {
        if (_.isNull(id) || !_.isNumber(id)) return false
        axios.get(base_path + 'pages/' + id)
            .then(response => {
                cb(response.data)
            })
            .catch(e => {
                cb(e)
            })
    },

    getPosts(limit, cb) {
        if (_.isEmpty(limit)) { let limit = 5 }

        axios.get(base_path + 'posts?per_page=' + limit)
            .then(response => {
                cb(response.data)
            })
            .catch(e => {
                cb(e)
            })
    },
}