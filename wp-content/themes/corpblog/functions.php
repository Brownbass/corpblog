<?php
// Remove all default WP template redirects/lookups
remove_action('template_redirect', 'redirect_canonical');

// Redirect all requests to index.php so the Vue app is loaded and 404s aren't thrown
function remove_redirects() {
    add_rewrite_rule('^/(.+)/?', 'index.php', 'top');
}
add_action('init', 'remove_redirects');

// Load scripts
function load_vue_scripts() {
    wp_enqueue_style('blankslate/app.css', get_template_directory_uri() . '/dist/styles/app.css', false, null);
    wp_enqueue_script('blankslate/manifest.js', get_template_directory_uri() . '/dist/scripts/manifest.js', null, null, true);
    wp_enqueue_script('blankslate/vendor.js', get_template_directory_uri() . '/dist/scripts/vendor.js', null, null, true);
    wp_enqueue_script('blankslate/app.js', get_template_directory_uri() . '/dist/scripts/app.js', null, null, true);
}

//add featured image support

add_action( 'after_setup_theme', 'corp_theme_setup' );
function corp_theme_setup() {
    add_theme_support( 'post-thumbnails');
}

//prepare rest

function post_featured_image_json( $data, $post, $context ) {
	$featured_image_id = $data->data['featured_media']; // get featured image id
	$featured_image_url = wp_get_attachment_image_src( $featured_image_id, 'original' ); // get url of the original size
	if( $featured_image_url ) {
		$data->data['featured_image_url'] = $featured_image_url[0];
	}
		
	return $data;
}

add_filter( 'rest_prepare_post', 'post_featured_image_json', 10, 3 );

// Remove tags from content

remove_filter( 'the_content', 'wpautop' );

remove_filter( 'the_excerpt', 'wpautop' );

//register menu

function register_my_menu() {
  register_nav_menu('primary-menu',__( 'Primary Menu' ));
}
add_action( 'init', 'register_my_menu' );

//load

add_action('wp_enqueue_scripts', 'load_vue_scripts', 100);
