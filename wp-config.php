<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'corpblog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y2St=*EVGS**$Y`H_1<Sl2NPg$jZJCk|+#{i`Zkz>/Pj}o-:r@v.lymp,w[>`A)]');
define('SECURE_AUTH_KEY',  '.;2r-eq[x7yF0W7{%wnT1S#Zwv?nPVjtO<pZb{=~o5Qyk_G+m1+:~dR]5locK@4$');
define('LOGGED_IN_KEY',    'SJO+_y*TFVWS252~tj/&w=/Dgbj>;l+yN0Yil678oVo!n&3-<]dF+/wYU8qd<zI;');
define('NONCE_KEY',        'T0nbH!J/4K0I[V[bvYtDvB-(g:+0K/(qJ:T~2l%}H6|^)RDa]n>b4}@S<j*xXgYY');
define('AUTH_SALT',        '$662dul17flz4)]~yW,@A/Y34;UGU2sLR]8:rJ(#_rd %{~^4yH_J3%AyyV[K2(k');
define('SECURE_AUTH_SALT', 'pgJW%y@V/G{zrA&9R]eiwbt2l;~;/bq]Cdl#5NW-HGCPz(U]WnX>&qdMqMrKHu2L');
define('LOGGED_IN_SALT',   'KY4=%l^@{+klZ3qA|lRu#{lQ9dCWiYlAC%ec)^]VU,XYq r,XK(,j ps^^Q|xs|q');
define('NONCE_SALT',       'v,:_7^+@5&x;JzgU-k?Fpu& unU9k^pp=4[8UF:Nca?{p.jrQvF4#0s|][<=q}9d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
